from multiprocessing import Pool
from io import BytesIO
import subprocess
from pybedtools import BedTool
from argparse import ArgumentParser
from functools import partial
from math import floor

def metilene(input_file, threads=1):
    with subprocess.Popen(('metilene', '--maxseg', '10000', '--threads', str(threads), input_file),
        stdout=subprocess.PIPE) as metilene:
            return BedTool(BytesIO(metilene.communicate()[0]))


def parse_arguments():
    parser = ArgumentParser(description='split metilene across chromosomes in parallel')
    parser.add_argument('input', metavar='<input.tsv>', nargs='+', help='input files')
    parser.add_argument('--processes', metavar='<int>', type=int, default=1, help='number of processes')
    return parser.parse_args()


def main():
    args = parse_arguments()
    if args.processes > 1:
        args.processes -= 1
    n_input = len(args.input)
    with Pool(processes=min(args.processes, n_input)) as pool:
        bedtools = pool.map(partial(metilene, threads=max(floor(args.processes/n_input), 1)), args.input)
    for bedtool in bedtools:
        print(bedtool, end='')

if __name__ == '__main__':
    main()
